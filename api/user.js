import { Router } from 'express';
import { User } from './model';
import { isBoolean } from './checktype'

const app = new Router();

const wrap = fn => (...args) => fn(...args).catch(args[2])

app.get('', wrap(async (req, res, next) => {
    var users = await User.findAll();
    res.json(users.map(user => {
        var userData = user.toJSON();
        delete userData.passowrd;
        return userData;
    }))
}))

app.put('/:userId', wrap(async (req, res, next) => {
    var user = await User.find({
        where: {
            id: req.params.userId
        }
    });

    if(!user)
        return res.status(400).json({message: 'user id not found'});

    var newStatus = {};

    if (req.body.isMod != undefined && isBoolean(req.body.isMod))
        newStatus.isMod = req.body.isMod;

    if (req.body.isStaff != undefined && isBoolean(req.body.isStaff))
        newStatus.isStaff = req.body.isStaff;

    await user.update(newStatus);
    var userData = user.toJSON();
    delete userData.passowrd;
    res.json(userData);
}))

export default app;