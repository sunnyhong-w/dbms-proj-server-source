import {Router} from 'express';
import { isInt, isBoolean } from './checktype';
import { Album, AlbumDiscount, Wishlist, Cart } from './model';
import { isStaff, isStaffOrMod } from './auth';
import SongRoute from './song';
import CommentRoute from './comment';
import Sequelize from 'sequelize';
const Op = Sequelize.Op;

const app = new Router();

const wrap = fn => (...args) => fn(...args).catch(args[2])
const pmap = (arr, fn) => Promise.all(arr.map(fn));

const CreateAlbumModelJSON = async (album) => {
    var albumData = album.toJSON();

    albumData.originalPrice = albumData.price;

    var discounts = await Album.getDiscounts(album.id);

    var discount = 1;
    for(var discountData of discounts)
        discount *= discountData.rate;
        
    albumData.price = Math.ceil(albumData.price * discount);
    albumData.discountRate = discount;

    return albumData;
}

app.get('', wrap(async (req, res, next)=> {
    var albums = await Album.findAll();
    var albumsJSON = await pmap(albums, album => CreateAlbumModelJSON(album));
    res.json(albumsJSON);
}))

app.post('', isStaff, wrap(async (req, res, next)=> {
    if(!req.body.name)
		return res.status(400).json({message: "name is required."});

    if(!req.body.price)
		return res.status(400).json({message: "price is required."});

    if(!isInt(req.body.price))
        return res.status(400).json({message: "album price should be interger more than 0 and less than 2147483647"});
    
    if(parseInt(req.body.price) < 0 || parseInt(req.body.price) > 2147483647)
        return res.status(400).json({message: "album price should be interger more than 0 and less than 2147483647"});

    if(req.body.available !== undefined && !isBoolean(req.body.available))
        return res.status(400).json({message: "album available should be boolean"});

    const albumData = {
        name: req.body.name,
        price: parseInt(req.body.price),
        available: req.body.available !== undefined ? req.body.available : false,
        description: req.body.description || undefined,
        username: req.session.status.me.username
    };

    try {
        const album = await Album.create(albumData);
        await album.setPublisher(req.session.status.me.id);
        return res.json(await CreateAlbumModelJSON(album));
    } catch (error) {
        console.log(error);
        return res.status(400).json({ message: "request may have type error" });
    }
}))

app.use('/:albumId', wrap(async (req, res, next)=> {
    var album = await Album.find({
        where: {
            id: req.params.albumId
        }
    });

    if(!album)
        return res.status(400).json({message: "album not found"});

    req.album = album;
    next();
}))

app.get('/:albumId', wrap(async (req, res, next)=> {
    res.json(await CreateAlbumModelJSON(req.album));
}))

app.put('/:albumId', isStaffOrMod, wrap(async (req, res, next)=> {
    if(req.body.name || req.body.price || req.body.description)
    {
        if(req.album.staffId !== req.session.status.me.id)
            return res.status(400).json({message: "Permission denied(Not Album's Staff)"});
    }
    
    if(req.body.available !== undefined)
    {
        if(req.album.staffId !== req.session.status.me.id && !req.session.status.me.isMod)
            return res.status(400).json({message: "Permission denied(Not Album's Staff and Not Moderator)"});
    }

    if(req.body.name)
        req.album.name = req.body.name;

    if(req.body.price)
    {
        if(isInt(req.body.price) && parseInt(req.body.price) > 0 && parseInt(req.body.price) < 2147483647)
            req.album.price = parseInt(req.body.price);
        else
            return res.status(400).json({message: "album price should be interger larger than 0"});
    }
    
    if(req.body.available !== undefined)
    {
        if(!isBoolean(req.body.available))
            return res.status(400).json({message: "album available should be boolean"});

        req.album.available = req.body.available;
    }

    if(req.body.description !== undefined)
        req.album.description = req.body.description;

    await req.album.save();

    if(req.body.available !== undefined && req.body.available === false)
    {
        await Wishlist.destroy({
            where: {
                albumId: req.album.id
            }
        });

        await Cart.destroy({
            where: {
                albumId: req.album.id
            }
        });
    }

    res.json(await CreateAlbumModelJSON(req.album));
}))

app.use('/:albumId/songs', SongRoute);
app.use('/:albumId/comments', CommentRoute);

export default app;
export { CreateAlbumModelJSON };