import { Router } from 'express';
import { isLoggedIn, isMod } from './auth';
import SessionRoute from './session';
import AlbumRoute from './album';
import CartRoute from './cart';
import WishlistRoute from './wishlist';
import UserRoute from './user';
import DiscountRoute from './discount';
import PurchaseInfoRoute from './purchaseInfo';

const app = new Router();

const wrap = fn => (...args) => fn(...args).catch(args[2])

app.use(wrap(async (req, res, next) => {
	if (!req.session.status) {
		req.session.status = {
			loggedIn: false,
			me: {}
		}
	}

	next();
}));


app.use('', SessionRoute);
app.use('/albums', AlbumRoute);
app.use('/cart', isLoggedIn, CartRoute);
app.use('/wishlist', isLoggedIn, WishlistRoute);
app.use('/users', isMod, UserRoute);
app.use('/discount', DiscountRoute);
app.use('/purchase', isLoggedIn, PurchaseInfoRoute);

app.use(function (req, res, next) {
	if (!req.route)
		return res.status(400).json({ message: "Unknown Request" });
	next();
});

export default app;