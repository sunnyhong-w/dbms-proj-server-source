import { Router } from 'express';
import { Song } from './model';
import { isLoggedIn ,isStaff } from './auth';
import SHA256 from 'sha256';
import fs from 'fs.promised';
import path from 'path';
import process from 'process';

const app = new Router();

const wrap = fn => (...args) => fn(...args).catch(args[2])

app.get('', wrap(async (req, res, next)=> {
    var songs = await req.album.getSongs();

    var users = await req.album.getPurchases({
        where: {
            id: req.session.status.me.id
        }
    });

    //TODO: test when user purchase / isMod
    var canAccessURL = users.length !== 0 || req.album.staffId === req.session.status.me.id || req.session.status.me.isMod;

    res.json(songs.map(song => {
        var s = song.toJSON();

        if (!canAccessURL)
            delete s.url;

        return s;
    }));
}))

app.post('', isStaff, wrap(async (req, res, next)=> {
    if(req.session.status.me.id !== req.album.staffId)
        return res.status(400).json({message: "Permission denied(Not Album's Staff)"});

    if (!req.files)
        return res.status(400).json({message: 'No files were uploaded.'});

    if(!req.body.name)
        return res.status(400).json({message: "name is required"});

    var songData = {};

    var file = req.files['file'];
    var albumpath = SHA256("" + req.album.id);
    var type = file.name.substr(file.name.lastIndexOf('.') + 1);

    if(type === 'mp3' || type === 'wav' || type === 'ogg' || type === 'flac' || type === 'midi')
    {
        songData.url = SHA256(`${file.name}${Date.now()}`);
        await file.mv(`./build/public/music/${songData.url}`);
        songData.filename = file.name;
    }
    else
        return res.status(400).json({message: "unsupport file type"});

    songData.name = req.body.name;

    var song = await Song.create(songData);
    await req.album.addSongs(song);
    res.json({
        ...song.toJSON(),
        albumId: req.album.id
    });
}))

app.use('/:songId', wrap(async (req, res, next)=> {
    var song = (await req.album.getSongs({
        where: {
            id: req.params.songId
        }
    }))[0];

    if(!song)
        return res.status(400).json({message: "song not found or wrong albumId"});

    req.song = song;
    next();
}))

app.get('/:songId', isLoggedIn, wrap(async (req, res, next)=> {
    var users = await req.album.getPurchases({
        where: {
            id: req.session.status.me.id
        }
    });

    if (users.length !== 0 || req.album.staffId === req.session.status.me.id || req.session.status.me.isMod) {
        const filepath = process.env.NODE_ENV === 'production' ? (
            path.resolve(__dirname, `public/music/${req.song.url}`)
        ) : (
            path.resolve(__dirname, `../../../build/public/music/${req.song.url}`)
        );
        res.download(filepath, req.song.filename);
    } else
        res.status(403).json({message: "Permission denied(Not purchase or not staff owner or not moderator)"});
}))

app.delete('/:songId', isStaff, wrap(async (req, res, next) => {
    if (req.album.staffId === req.session.status.me.id)
    {
        var url = req.song.url;
        await req.song.destroy();
        await fs.unlink(path.resolve(__dirname, `../../../build/public/music/${url}`));
        req.song = undefined;
        res.json({});
    }
    else
        res.status(400).json({ message: "Permission denied(Not staff owner)" });
}))

export default app;