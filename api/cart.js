import { Router } from 'express';
import { User, Cart, Album, Purchase, Wishlist, PurchaseInfo } from './model';
import { CreateAlbumModelJSON } from './album';
import { GetPurchaseInfoModalJSON } from './purchaseInfo';
import Sequelize from 'sequelize';
const Op = Sequelize.Op;

const app = new Router();

const wrap = fn => (...args) => fn(...args).catch(args[2])
const pmap = (arr, fn) => Promise.all(arr.map(fn));

const CreateCartAlbumModelJSON = async album => {
    var cartData = await CreateAlbumModelJSON(album);
    delete cartData.cart;
    return cartData;
}

const GetAlbumsInCarts = async (req, user) => {
    var carts = await user.getCarts();
    var cartAlbums = await pmap(carts, album => CreateCartAlbumModelJSON(album)); //get album with discount info in JSON obj
    return cartAlbums;
}

const GetAlbumsInPurchases = async (req, user) => {
    var purchases = (await user.getPurchases()).map(purchase => {
        var purchaseData = purchase.toJSON();
        purchaseData.price = purchaseData.purchase.total;
        purchaseData.originalPrice = purchaseData.purchase.subtotal;
        delete purchaseData.purchase;
        return purchaseData;
    });
    return purchases;
}

app.get('', wrap(async (req, res, next) => {
    var user = await User.find({
        where: {
            id: req.session.status.me.id
        }
    });

    res.json(await GetAlbumsInCarts(req, user));
}))

app.post('', wrap(async (req, res, next) => {
    try 
    {
        var albums = await Album.findAll({
            where: {
                id: {
                    [Op.in]: req.body
                },
                available: true
            }
        });

        if (albums.length !== req.body.length)
            return res.status(400).json({ message: 'request include invaild album id or include not avaliable album' });
    } 
    catch (error) 
    {
        return res.status(400).json({ message: 'request include invaild album id (might be id type error)' });
    }

    var purchases = await Purchase.findAll({
        where: {
            albumId: {
                [Op.in]: req.body
            },
            userId: req.session.status.me.id
        }
    });

    if(purchases.length !== 0)
        return res.status(400).json({ message: 'request include item already purchased' });

    var user = await User.find({
        where: {
            id: req.session.status.me.id
        }
    });

    //sequalizeJS seem to work thing strange, this give me array of array of instance again...
    //hacked so it act perfect, there might be something wrong of sequalizeJS I think(or maybe I done something wrong?)
    var carts = (await user.addCarts(req.body))[0];

    if(carts)
        return res.json(await GetAlbumsInCarts(req, user));
    else
        return res.status(400).json({ message: 'all album is already in cart' });
}))


app.delete('/:albumId', wrap(async (req, res, next) => {
    var cart = await Cart.find({
        where: {
            albumId: req.params.albumId,
            userId: req.session.status.me.id
        }
    });

    if (!cart)
        return res.status(400).json({ message: "Album is not in cart" });

    await cart.destroy();
    res.json({});
}))

const CalculateFee = (subtotal) =>
{
    return subtotal > 1500 ? 0 : 30;
}


app.post('/checkout', wrap(async (req, res, next) => {
    var user = await User.find({
        where: {
            id: req.session.status.me.id
        }
    });

    var carts = await user.getCarts();
    var purchaseInfo = {
        total: 0,
        subtotal: 0,
        fee: 0,
        details: []
    };

    var purchaseData = await pmap(carts, async album => { 
        var albumData = await CreateCartAlbumModelJSON(album);

        Wishlist.destroy({
            where: {
                albumId: albumData.id,
                userId: req.session.status.me.id
            }
        });

        delete albumData.available;

        purchaseInfo.details.push(albumData);
        purchaseInfo.subtotal += albumData.price;

        return {
            total: albumData.price,
            subtotal: albumData.originalPrice,
            albumId: albumData.id,
            userId: req.session.status.me.id
        }
    }); //use this to calculate discounted price

    purchaseInfo.fee = CalculateFee(purchaseInfo.subtotal);

    purchaseInfo.total = purchaseInfo.subtotal + purchaseInfo.fee;
    purchaseInfo.details = JSON.stringify(purchaseInfo.details);

    await Purchase.bulkCreate(purchaseData);

    await Cart.destroy({
        where: {
            userId: req.session.status.me.id
        }
    });

    var purchaseResult = await PurchaseInfo.create(purchaseInfo);
    user.addPurchaseInfo(purchaseResult);

    res.json(GetPurchaseInfoModalJSON(purchaseResult));
}))

app.get('/calculate', wrap(async (req, res, next) => {
    var user = await User.find({
        where: {
            id: req.session.status.me.id
        }
    });

    var carts = await user.getCarts();
    var purchaseInfo = {
        total: 0,
        subtotal: 0,
        fee: 0,
        details: []
    };

    var purchaseData = await pmap(carts, async album => {
        var albumData = await CreateCartAlbumModelJSON(album);

        delete albumData.available;

        purchaseInfo.details.push(albumData);
        purchaseInfo.subtotal += albumData.price;

        return {
            total: albumData.price,
            subtotal: albumData.originalPrice,
            albumId: albumData.id,
            userId: req.session.status.me.id
        }
    }); //use this to calculate discounted price

    purchaseInfo.fee = CalculateFee(purchaseInfo.subtotal);

    purchaseInfo.total = purchaseInfo.subtotal + purchaseInfo.fee;
    purchaseInfo.details = purchaseInfo.details;

    res.json(purchaseInfo);
}))

export default app;
export { GetAlbumsInCarts, GetAlbumsInPurchases };