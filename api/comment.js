import { Router } from 'express';
import { Comment, Purchase } from './model';
import { isLoggedIn, isMod } from './auth';
import { isInt } from './checktype';
import songRoute from './song';

const app = new Router();

const wrap = fn => (...args) => fn(...args).catch(args[2])

app.get('', wrap(async (req, res, next) => {
    var commenters = await req.album.getComments();
    res.json(commenters.map(commenter => {
        var comment = commenter.comment.toJSON();
        comment.username = commenter.username;
        delete comment.userId;
        return comment;
    }));
}))

app.post('', isLoggedIn, wrap(async (req, res, next) => {

    if (!req.body.body)
        return res.status(400).json({ message: "body is required." });

    if (!req.body.rate)
        return res.status(400).json({ message: "rate is required." });

    if(!isInt(req.body.rate))
        return res.status(400).json({ message: "rate should be interger." });
    
    if(req.body.rate < 0 || req.body.rate > 5)
        return res.status(400).json({ message: "rate should be between 0 and 5." });
    
    var isCommented = await Comment.find({
        where: {
            userId: req.session.status.me.id,
            albumId: req.album.id
        }
    });
    
    var isPurchased = await Purchase.find({
        where: {
            userId: req.session.status.me.id,
            albumId: req.album.id
        }
    });

    if(!isPurchased)
        return res.status(400).json({ message: "album not purchased, can not comment" });

    if(isCommented)
        return res.status(400).json({ message: "it's already commented." });

    //sequelizeJS said it should return an array of instance but I kept getting array of array of instance so I jsut hacked like this.
    var comments = (await req.album.addComment(req.session.status.me.id, {
        through: {
            body: req.body.body,
            rate: req.body.rate
        }
    }))[0][0];

    var comment = comments.toJSON();
    comment.username = req.session.status.me.username;
    delete comment.userId;

    res.json(comment); 
}))

app.delete('/:commentId', wrap(async (req, res, next) => {
    var comment = await Comment.find({
        where: {
            id: req.params.commentId
        }
    });

    if (!comment)
        return res.status(400).json({ message: "comment not found." });

    if(!req.session.status.me.isMod && comment.userId !== req.session.status.me.id)
        return res.status(400).json({ message: "Permission denied(Not Commenter or Moderator)" });

    if (req.album.id !== comment.albumId)
        return res.status(400).json({ message: "comment not belongs to current album" });

    await comment.destroy();
    res.json({});
}))

export default app;