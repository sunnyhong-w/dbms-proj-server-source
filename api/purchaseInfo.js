import { Router } from 'express';
import { PurchaseInfo } from './model';

const app = new Router();

const wrap = fn => (...args) => fn(...args).catch(args[2])

const GetPurchaseInfoModalJSON = (purchaseInfo) => {
    var purchaseResult = purchaseInfo.toJSON();
    purchaseResult.details = JSON.parse(purchaseResult.details);
    return purchaseResult;
}

app.get('/', wrap(async (req, res, next) => {
    var purchaseInfos = await PurchaseInfo.findAll({
        where: {
            userId: req.session.status.me.id
        }
    });

    if (purchaseInfos)
        return res.json(purchaseInfos.map(purchaseInfo => GetPurchaseInfoModalJSON(purchaseInfo)));
    else
        return res.json([]);
}))

app.get('/:pid', wrap(async (req, res, next) => {
    var purchaseInfo = await PurchaseInfo.find({
        where: {
            id: req.params.pid,
            userId: req.session.status.me.id
        }
    });

    if (purchaseInfo)
        res.json(GetPurchaseInfoModalJSON(purchaseInfo));
    else
        res.status(400).json({ 'message': 'purchaseInfo not found or user is not the purchaser'});
}));

export default app;
export { GetPurchaseInfoModalJSON };