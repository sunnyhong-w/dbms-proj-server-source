import { Router } from 'express';
import { isStaffOrMod } from './auth';
import { Album, Discount, AlbumDiscount } from './model';
import Sequelize from 'sequelize';
const Op = Sequelize.Op;

const app = new Router();

const wrap = fn => (...args) => fn(...args).catch(args[2])

app.get('/', wrap(async (req, res, next) => {
    if (req.query.type === 'seasonDiscount')
    {
        var discounts = await Discount.findAll({
            where: {
                type: 'Season Discount'
            }
        })

        return res.json(discounts.map(discount => discount.toJSON()));
    }
    else if (req.query.type === 'specialEvent')
    {
        if (!req.session.status.me.isStaff)
            return res.status(400).json({ message: 'you should be staff to get all specialEvent which hold by you' });

        var albums = await Album.findAll({
            where: {
                username: req.session.status.me.username
            }
        });

        var adList = await AlbumDiscount.findAll({
            where: {
                albumId: {
                    [Sequelize.Op.in]: albums.map(album => album.id)
                }
            }
        });

        var discounts = await Discount.findAll({
            where: { 
                id: { [Sequelize.Op.in]: adList.map(ad => ad.discountId) }, 
                type: 'Special Event' 
            }
        });

        var discountInfo = [];

        for (var discount of discounts)
        {
            var discountedAlbum = await AlbumDiscount.findAll({
                where: {
                    discountId: discount.id
                }
            });

            var info = discount.toJSON();
            info.albums = discountedAlbum.map(ad => ad.albumId);
            discountInfo.push(info);
        }

        return res.json(discountInfo);
    }
    else
        return res.status(400).json({ message: 'unknow type of discount' });
}))

app.get('/:albumId', wrap(async (req, res, next) => {
    try {
        var discounts = await Album.getDiscounts(req.params.albumId, req.query.available ? true : false);
        res.json(discounts.map(discount => discount.toJSON()));
    }
    catch (error) {
        return res.status(400).json({ message: 'request albumId invaild' });
    }
}))

app.post('', isStaffOrMod, wrap(async (req, res, next) => {
    if (!req.body.rate || req.body.rate > 1 || req.body.rate <= 0)
        return res.status(400).json({ message: 'request discount rate setting error' });

    if (!req.body.dateFrom)
        return res.status(400).json({ message: 'request discount dateFrom require' });

    if (!req.body.dateTo)
        return res.status(400).json({ message: 'request discount dateTo require' });

    if (!req.body.type)
        return res.status(400).json({ message: 'request discount type require' });

    if (req.body.type !== 'Special Event' && req.body.type !== 'Season Discount')
        return res.status(400).json({ message: 'undefinded discount type' });

    if (req.body.type === 'Special Event' && req.session.status.me.isStaff === false)
        return res.status(400).json({ message: 'you should be staff of album to set special event' });

    if (req.body.type === 'Season Discount' && req.session.status.me.isMod === false)
        return res.status(400).json({ message: 'you should be mod to set season discount' });

    try {
        if (req.body.type === 'Special Event')
        {
            var where = {
                id: {
                    [Op.in]: req.body.albums
                }
            };


            where.staffId = req.session.status.me.id;

            var albums = await Album.findAll({ where });

            if (albums.length !== req.body.albums.length)
                return res.status(400).json({ message: 'request include invaild album id (Note: you should be staff of album to set special event)' });
        }
    }
    catch (error) {
        return res.status(400).json({ message: 'request include invaild album id' });
    }

    var discountObj = {
        dateTo: req.body.dateTo,
        dateFrom: req.body.dateFrom,
        rate: req.body.rate,
        type: req.body.type
    }

    var discount = await Discount.create(discountObj);

    if (discount)
    {

        if (req.body.type === 'Special Event')
        {
            AlbumDiscount.bulkCreate(req.body.albums.map(albumId => ({
                discountId: discount.id,
                albumId
            })))
        }

        return res.json({ message: 'add discount success'});
    }
    else
        return res.status(400).json({ message: 'add discount fail' });
}))


app.delete('/:discountId', isStaffOrMod, wrap(async (req, res, next) => {
    var discount = await Discount.find({
        where: {
            id: req.params.discountId
        }
    });

    if (!discount)
        return res.status(400).json({ message: "Discount not found" });

    if (discount.type === 'Season Discount' && req.session.status.me.isMod === false)
        return res.status(400).json({ message: 'you should be mod to remove season discount' });

    if (discount.type === 'Special Event' && req.session.status.me.isStaff === false)
        return res.status(400).json({ message: 'you should be staff of album to remove special event' });
    
    var discountId = discount.id;

    if (discount.type === 'Special Event')
    {
        var ads = await AlbumDiscount.findAll({
            where: {
                discountId: req.params.discountId
            }
        })

        var albums = await Album.findAll({
            where: {
                id: {
                    [Sequelize.Op.in]:  ads.map(ad => ad.albumId)
                }
            }
        });

        if (albums.length != ads.length)
            return res.status(400).json({ message: 'discount album not found, there might be database or backend error.' });

        for(var album of albums)
        {
            if (album.staffId != req.session.status.me.id)
                return res.status(400).json({ message: 'you should be staff of this album to remove special event' });
        }

        await discount.destroy();
        await AlbumDiscount.destroy({
            where: {
                discountId: req.params.discountId
            }
        });
        
    }
    else
        await discount.destroy();

    res.json({});
}))

export default app;