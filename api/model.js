import Sequelize from 'sequelize';
import config from '../../../config';
import SHA256 from 'sha256';

const sequelize = new Sequelize(config.db.schemaName, config.db.username , config.db.password, {
  host: 'localhost',
  dialect: 'mysql',

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },

  define: {
    charset: 'utf8',
    collate: 'utf8_general_ci'
  },

  // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
  operatorsAliases: false
});
 
const Album = sequelize.define('album',{
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    price: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    available: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    description: Sequelize.STRING,
    username: Sequelize.STRING
});

const User = sequelize.define('user', {
    username: {
        type: Sequelize.STRING,
        allowNull: false
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    },
    sex: {
        type: Sequelize.ENUM('Male', 'Female'),
        allowNull: false
    },
    isStaff: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    isMod: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    fullName: Sequelize.STRING,
    birthday: Sequelize.DATE,
    email: Sequelize.STRING,
    phone: Sequelize.STRING
});

const PurchaseInfo = sequelize.define('purchase_info', {
    fee: Sequelize.INTEGER,
    subtotal: Sequelize.INTEGER,
    total: Sequelize.INTEGER,
    details: Sequelize.TEXT
});

const Discount = sequelize.define('discount', {
    rate: Sequelize.FLOAT,
    type: Sequelize.ENUM('Special Event', 'Season Discount'),
    dateFrom: Sequelize.DATE,
    dateTo: Sequelize.DATE
})

const AlbumDiscount = sequelize.define('albumDiscount', {
    discountId: Sequelize.INTEGER,
    albumId: Sequelize.INTEGER
})

const Purchase = sequelize.define('purchase', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    total: Sequelize.INTEGER,
    subtotal: Sequelize.INTEGER
});

const Cart = sequelize.define('cart', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    }
});

const Wishlist = sequelize.define('wishlist', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    }
});

const Comment = sequelize.define('comment', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    body: {
        type: Sequelize.STRING,
        allowNull: false
    },
    rate: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
});

const Song = sequelize.define('song', {
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    url: Sequelize.STRING,
    filename: {
        type: Sequelize.STRING,
        allowNull: false
    }
});

User.belongsToMany(Album, {
    through: Purchase,
    foreignKey: 'userId',
    as: "purchases"
});

Album.belongsToMany(User, {
    through: Purchase,
    foreignKey: 'albumId',
    as: "purchases"
});


User.belongsToMany(Album, {
    through: Cart,
    foreignKey: 'userId',
    as: "carts"
});

Album.belongsToMany(User, {
    through: Cart,
    foreignKey: 'albumId',
    as: "carts"
});


User.belongsToMany(Album, {
    through: Wishlist,
    foreignKey: 'userId',
    as: "wishlists"
});

Album.belongsToMany(User, {
    through: Wishlist,
    foreignKey: 'albumId',
    as: "wishlists"
});


User.belongsToMany(Album, {
    through: Comment,
    foreignKey: 'userId',
    as: "comments"
});

Album.belongsToMany(User, {
    through: Comment,
    foreignKey: 'albumId',
    as: "comments"
});


Album.hasMany(Song, {
    foreignKey: 'albumId',
    as: 'songs'
});

Song.belongsTo(Album, {
    foreignKey: 'albumId',
    as: 'includeIn'
});


User.hasMany(Album, {
    foreignKey: 'staffId',
    as: 'publishes'
})

Album.belongsTo(User, {
    foreignKey: 'staffId',
    as: 'publisher'
})

User.hasMany(PurchaseInfo, {
    foreignKey: 'userId',
    as: "purchaseInfo"
})

PurchaseInfo.belongsTo(User, {
    foreignKey: 'userId',
    as: "purchaseBy"
})

Album.getDiscounts = async (albumId, available = true) => {

    var adList = await AlbumDiscount.findAll({
        where: {
            albumId
        }
    });

    var additionalQuery = {};
    if (available)
    {
        additionalQuery = {
            dateFrom: {
                [Sequelize.Op.lt]: Date.now()
            },
            dateTo: {
                [Sequelize.Op.gt]: Date.now()
            }
        }
    }

    var discounts = await Discount.findAll({
        where: {
            [Sequelize.Op.or]: [
                { id: { [Sequelize.Op.in]: adList.map(ad => ad.discountId) }, type: 'Special Event' },
                { type: 'Season Discount' }
            ],
            ...additionalQuery
        }
    })

    return discounts;
}

const SyncDB = async (force = false) => {
    await sequelize.sync({force});

    if(force)
    {
        await User.create({
            username: config.mod.username,
            password: SHA256(config.mod.password),
            sex: 'Male',
            isStaff: true,
            isMod: true
        });
    }
}

export { Album, User, Discount, AlbumDiscount, Purchase, Cart, Wishlist, Comment, Song, PurchaseInfo, SyncDB };