const wrap = fn => (...args) => fn(...args).catch(args[2])

const isLoggedIn = wrap(async (req, res, next)=>{
    if(req.session.status.loggedIn)
        next();
    else
        res.status(400).json({"message": "Permission denied(Not logged in)"});
});

const isStaff = wrap(async (req, res, next)=>{
    if(req.session.status.loggedIn && req.session.status.me.isStaff)
        next();
    else
        res.status(400).json({"message": "Permission denied(Not Staff)"});
});

const isMod = wrap(async (req, res, next)=>{
    if(req.session.status.loggedIn && req.session.status.me.isMod)
        next();
    else
        res.status(400).json({"message": "Permission denied(Not Moderator)"});
});

const isStaffOrMod = wrap(async (req, res, next)=>{
    if(req.session.status.loggedIn && (req.session.status.me.isStaff || req.session.status.me.isMod))
        next();
    else
        res.status(400).json({"message": "Permission denied(Not Staff or Moderator)"});
});

export { isLoggedIn, isStaff, isMod, isStaffOrMod };