import { Router } from 'express';
import { User, Album, Wishlist, Purchase } from './model';
import { CreateAlbumModelJSON } from './album';
import Sequelize from 'sequelize';
const Op = Sequelize.Op;

const app = new Router();

const wrap = fn => (...args) => fn(...args).catch(args[2])
const pmap = (arr, fn) => Promise.all(arr.map(fn));

const GetAlbumsInWishlist = async (req, user) => {
    var wishlists = await user.getWishlists();

    var wishlistAlbums = await pmap(wishlists, async album => {
        var wishlistData = await CreateAlbumModelJSON(album);
        delete wishlistData.wishlist;
        return wishlistData;
    });

    return wishlistAlbums;
}

app.get('', wrap(async (req, res, next) => {
    var user = await User.find({
        where: {
            id: req.session.status.me.id
        }
    });

    res.json(await GetAlbumsInWishlist(req, user));
}))

app.post('', wrap(async (req, res, next) => {
    try 
    {
        var albums = await Album.findAll({
            where: {
                id: {
                    [Op.in]: req.body
                },
                available: true
            }
        });

        if (albums.length !== req.body.length)
            return res.status(400).json({ message: 'request include invaild album id or include not avaliable album' });
    }
    catch (error) 
    {
        console.log(error);
        return res.status(400).json({ message: 'request include invaild album id (might be id type error)' });
    }

    var purchases = await Purchase.findAll({
        where: {
            albumId: {
                [Op.in]: req.body
            },
            userId: req.session.status.me.id
        }
    });

    if (purchases.length !== 0)
        return res.status(400).json({ message: 'request include item already purchased' });

    var user = await User.find({
        where: {
            id: req.session.status.me.id
        }
    });

    //array of array of instance again... hacked.
    var wishlists = (await user.addWishlists(req.body))[0];

    if (wishlists)
        return res.json(await GetAlbumsInWishlist(req, user));
    else
        return res.status(400).json({ message: 'all album is already in wishlist' });
}))

app.delete('/:albumId', wrap(async (req, res, next) => {
    var wishlist = await Wishlist.find({
        where: {
            albumId: req.params.albumId,
            userId: req.session.status.me.id
        }
    });

    if (!wishlist)
        return res.status(400).json({ message: "Album is not in wishlist" });

    await wishlist.destroy();
    res.json({});
}))

export default app;
export { GetAlbumsInWishlist }