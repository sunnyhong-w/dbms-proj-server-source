import { Router } from 'express';
import { User } from './model';
import { GetAlbumsInCarts, GetAlbumsInPurchases } from './cart';
import { GetAlbumsInWishlist } from './wishlist';
import { isLoggedIn } from './auth'
import SHA256 from 'sha256';

const app = new Router();

const wrap = fn => (...args) => fn(...args).catch(args[2])

app.get('/status', wrap(async (req, res, next) => {
    if (req.session.status.loggedIn)
    {
        var user = await User.find({
            where: req.session.user
        });

        var cart = await GetAlbumsInCarts(req, user);
        var purchased = await GetAlbumsInPurchases(req, user);
        var wishlist = await GetAlbumsInWishlist(req, user);

        return res.json({
            ...req.session.status,
            cart,
            purchased,
            wishlist
        });
    }
    else
    {
        return res.json({
            ...req.session.status,
            cart: [],
            purchased: [],
            wishlist: []
        });
    }

}))

app.post('/register', wrap(async (req, res, next) => {
    if (req.session.status && req.session.status.loggedIn)
        return res.status(400).json({ message: "you're already logged in." });

    if (!req.body.username)
        return res.status(400).json({ message: "username is required." });

    if (!req.body.password)
        return res.status(400).json({ message: "password is required." });

    if (!req.body.sex)
        return res.status(400).json({ message: "sex is required." });

    if (req.body.sex !== 'Male' && req.body.sex !== 'Female')
        return res.status(400).json({ message: "sex must be 'Male'  or 'Female'." });

    var user = await User.find({
        where: {
            username: req.body.username
        }
    });

    if (!user) {
        var body = {
            username: req.body.username,
            password: SHA256(req.body.password),
            sex: req.body.sex,
            isStaff: req.body.isStaff ? req.body.isStaff : false,
            isMod: false,
            fullName: req.body.fullName || undefined,
            birthday: req.body.birthday ? new Date(req.body.birthday) : undefined,
            email: req.body.email || undefined,
            phone: req.body.phone || undefined,
        };

        var newuser = await User.create(body);

        var me = newuser.toJSON();
        delete me.password;

        req.session.status = {
            loggedIn: true,
            me
        };

        req.session.user = {
            username: req.body.username,
            password: SHA256(req.body.password)
        }

        res.json({
            ...req.session.status,
            cart: [],
            purchased: [],
            wishlist: []
        });
    }
    else
        res.status(400).json({ message: "Username is used, please try another one." });
}))

app.post('/login', wrap(async (req, res, next) => {
    if (req.session.status && req.session.status.loggedIn)
        return res.status(400).json({ message: "you're already logged in." });

    if (!req.body.username)
        return res.status(400).json({ message: "username is required." });

    if (!req.body.password)
        return res.status(400).json({ message: "password is required." });

    var user = await User.find({
        where: {
            username: req.body.username,
            password: SHA256(req.body.password)
        }
    });

    if (user) {
        var me = user.toJSON();
        delete me.password;

        req.session.status = {
            loggedIn: true,
            me
        };

        req.session.user = {
            username: req.body.username,
            password: SHA256(req.body.password)
        }
        
        var cart = await GetAlbumsInCarts(req, user);
        var purchased = await GetAlbumsInPurchases(req, user);
        var wishlist = await GetAlbumsInWishlist(req, user);
        
        res.json({
            ...req.session.status,
            cart,
            purchased,
            wishlist
        });
    }
    else
        res.status(400).json({ message: "Username not found or Password error." });

}))

app.post('/logout', wrap(async (req, res, next) => {
    req.session.status = {
        loggedIn: false,
        me: {}
    };

    res.json({
        ...req.session.status,
        cart: [],
        purchased: [],
        wishlist: []
    });
}))

export default app;