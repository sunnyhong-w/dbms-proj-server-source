const isNumber = (val) => !isNaN(val * 1);
const isInt = (val) => isNumber(val) && Math.floor(val) === parseFloat(val);
const isBoolean = (val) => (typeof val) === 'boolean';

export {isNumber, isInt, isBoolean}