import express from 'express';
import session from 'express-session';
import FileUpload from 'express-fileupload';
import bodyParser from 'body-parser';
import config from '../../config';
import path from 'path';

import api from './api';

import { SyncDB } from './api/model';

const app = express();

if(process.env.NODE_ENV === 'development') {
	const webpack = require('webpack');
	const devMiddleware = require('webpack-dev-middleware');
	const hotMiddleware = require('webpack-hot-middleware');

	const clientWebpackConfig = require('../../webpack/client');

	const compiler = webpack(clientWebpackConfig);

	app.use(devMiddleware(compiler, {
			noInfo: false,
			publicPath: '/build/'
	}));

	app.use(hotMiddleware(compiler));
}

app.use(session({
	secret: 'bigboat',
	resave: false,
	saveUninitialized: true
}));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
	extended: false
}));

// parse application/json
app.use(bodyParser.json());

app.use(FileUpload({
  limits: { fileSize: 50 * 1024 * 1024 },
}));

app.use('/api', api);

app.use(express.static(path.resolve(__dirname, 'public')));

app.use('*', (req, res) => {
	res.sendFile(path.resolve(__dirname, 'public/index.html'));
});

(async () => {
	await SyncDB(config.db.forceSync);

	if (config.enableHttps) {
		const fs = require('fs');
		const http = require('http');
		const https = require('https');
		const privateKey = fs.readFileSync(path.resolve(__dirname,
			process.env.NODE_ENV === 'production' ? '../ssl/server.key' : '../../ssl/server.key'), 'utf8');
		const certificate = fs.readFileSync(path.resolve(__dirname,
			process.env.NODE_ENV === 'production' ? '../ssl/server.crt' : '../../ssl/server.crt'), 'utf8');
		const credentials = {key: privateKey, cert: certificate};
		const httpServer = http.createServer(app);
		const httpsServer = https.createServer(credentials, app);

		httpServer.listen(config.port, () => {
			console.log('HTTP server is listening on %d', config.port);
		});
		httpsServer.listen(config.httpsPort, () => {
			console.log('HTTP server is listening on %d', config.httpsPort);
		});
	} else {
		app.listen(config.port, () => {
			console.log('app listening on %d', config.port);
		});
	}
})();

export default app;